use bevy::prelude::*;

pub enum MinoColor {
    // RGB
    Yellow,    // FFFF00 1.0 1.0 0.0
    LightBlue, // 00BFFF 0.0 0.749 1.0
    Purple,    // 800080 0.502, 0.0, 0.502
    Orange,    // FFA500 1.0, 0.647, 0.0
    DarkBlue,  //00008B 0.0 0.0 0.545
    Green,     // 32CD32 0.196, 0.804, 0.196
    Red,       // FF0000 1.0, 0.0, 0.0
}

impl MinoColor {
    pub fn color(&self) -> Color {
        match self {
            MinoColor::Yellow => Color::rgb(1.0, 1.0, 0.0),
            MinoColor::LightBlue => Color::rgb(0.0, 0.749, 1.0),
            MinoColor::Purple => Color::rgb(0.502, 0.0, 0.502),
            MinoColor::Orange => Color::rgb(1.0, 0.647, 0.0),
            MinoColor::DarkBlue => Color::rgb(0.0, 0.2, 0.645),
            MinoColor::Green => Color::rgb(0.196, 0.804, 0.196),
            MinoColor::Red => Color::rgb(1.0, 0.0, 0.0),
        }
    }
}

#[derive(Component, Debug)]
struct Mino;

pub fn mino_bundle(
    position_x: f32,
    position_y: f32,
    size: f32,
    mesh_distance: f32,
    offset_x: f32,
    offset_y: f32,
    color: MinoColor,
) -> SpriteBundle {
    SpriteBundle {
        sprite: Sprite {
            color: color.color(),
            ..Default::default()
        },
        transform: Transform {
            scale: Vec3::new(size, size, 0.0),
            translation: Vec3::new(
                position_x * mesh_distance + offset_x,
                position_y * mesh_distance + offset_y,
                1.0,
            ),
            ..Default::default()
        },
        global_transform: GlobalTransform::default(),
        ..Default::default()
    }
}
