mod arena;
mod mino;
mod movement;
mod tetrimino;

use crate::arena::*;
use crate::movement::*;
use crate::tetrimino::*;
use bevy::core::FixedTimestep;
use bevy::prelude::*;

fn setup_camera(mut commands: Commands) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
}

fn main() {
    let window = WindowDescriptor {
        title: String::from("Tetris"),
        width: 1000.0,
        height: 1000.0,
        resizable: false,
        ..Default::default()
    };

    App::new()
        .insert_resource(window)
        .insert_resource(FallTimer::create())
        .add_startup_system(setup_camera)
        .add_startup_system(spawn_background_image)
        .add_startup_system(spawn_tetris_field)
        .add_startup_system(spawn_tetrimino_base)
        .add_startup_system(spawn_next_tetrimino_base)
        .add_startup_system(spawn_text_ui)
        .add_system(mino_movement)
        .add_system(spawn_tetrimino)
        .add_system(spawn_next_tetrimino)
        .add_system(display_blocks)
        .add_system(position_translation)
        .add_system(text_update_system)
        .add_system(mino_falling)
        .add_plugins(DefaultPlugins)
        .run();
}
