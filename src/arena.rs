use crate::mino::mino_bundle;
use crate::tetrimino::*;
use crate::{Facing, IsChanged, Stone, TetriminoBag};
use bevy::prelude::*;
use simple_grid::*;

pub const ARENA_OFFSET_X: f32 = -431.0;
pub const ARENA_OFFSET_Y: f32 = -431.0;
pub const ARENA_WIDTH: u8 = 10;
pub const ARENA_HEIGHT: u8 = 24;

#[derive(Component)]
pub struct TetrisField;

#[derive(Component)]
struct BackGroundImage;

#[derive(Component)]
pub struct TetriminoText;

pub fn spawn_background_image(mut commands: Commands, asset_server: Res<AssetServer>) {
    let background_image: Handle<Image> = asset_server.load("Grid.png");
    commands
        .spawn_bundle(SpriteBundle {
            texture: background_image,
            ..Default::default()
        })
        .insert(BackGroundImage);
}

pub fn spawn_tetris_field(mut commands: Commands) {
    commands
        .spawn_bundle(SpriteBundle {
            ..Default::default()
        })
        .insert(TetrisField)
        .insert(Board::new(ARENA_WIDTH, ARENA_HEIGHT))
        .insert(IsChanged { is_changed: false })
        .id();
}

pub fn display_blocks(
    mut commands: Commands,
    mut query: Query<(&Board, &IsChanged, Entity), With<TetrisField>>,
) {
    for (board, is_changed, parent) in query.iter_mut() {
        if is_changed.is_changed {
            commands.entity(parent).despawn_descendants();
            for column in 0..ARENA_WIDTH {
                for row in 0..ARENA_HEIGHT {
                    match board.data.get((column as usize, row as usize)).unwrap() {
                        Some(t) => {
                            commands
                                .spawn_bundle(mino_bundle(
                                    column as f32,
                                    row as f32,
                                    29.0,
                                    35.0,
                                    ARENA_OFFSET_X,
                                    ARENA_OFFSET_Y,
                                    t.color(),
                                ))
                                .insert(Parent(parent));
                        }
                        None => {}
                    }
                }
            }
        }
    }
}

pub fn spawn_text_ui(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn_bundle(UiCameraBundle::default());
    commands
        .spawn_bundle(TextBundle {
            style: Style {
                align_self: AlignSelf::FlexEnd,
                ..Default::default()
            },
            // Use `Text` directly
            text: Text {
                // Construct a `Vec` of `TextSection`s
                sections: vec![
                    TextSection {
                        value: "Tetrimino: ".to_string(),
                        style: TextStyle {
                            font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                            font_size: 60.0,
                            color: Color::WHITE,
                        },
                    },
                    TextSection {
                        value: "".to_string(),
                        style: TextStyle {
                            font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                            font_size: 60.0,
                            color: Color::GOLD,
                        },
                    },
                    TextSection {
                        value: "\nRotation: ".to_string(),
                        style: TextStyle {
                            font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                            font_size: 60.0,
                            color: Color::WHITE,
                        },
                    },
                    TextSection {
                        value: "".to_string(),
                        style: TextStyle {
                            font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                            font_size: 60.0,
                            color: Color::GOLD,
                        },
                    },
                    TextSection {
                        value: "\nNextTetrimino: ".to_string(),
                        style: TextStyle {
                            font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                            font_size: 60.0,
                            color: Color::WHITE,
                        },
                    },
                    TextSection {
                        value: "".to_string(),
                        style: TextStyle {
                            font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                            font_size: 60.0,
                            color: Color::GOLD,
                        },
                    },
                ],
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(TetriminoText);
}

pub fn text_update_system(
    mut text_query: Query<&mut Text, With<TetriminoText>>,
    mut tetrimino_query: Query<(&TetriminoBag, &Facing), With<Stone>>,
) {
    let mut text = text_query.iter_mut().next().unwrap();
    let (tetrimino, direction) = tetrimino_query.iter_mut().next().unwrap();
    text.sections[1].value = format!("{:?}", &tetrimino.get_current_tetrimino());
    text.sections[3].value = format!("{:?}", &direction.direction);
    text.sections[5].value = format!("{:?}", &tetrimino.get_rest_bag());
}

#[derive(Component, PartialEq, Eq, Debug)]
pub struct Board {
    pub width: u8,
    pub height: u8,
    pub data: Grid<Option<TetriminoVariant>>,
}

impl Board {
    pub fn new(width: u8, height: u8) -> Self {
        let data = Grid::new(
            width as usize,
            height as usize,
            vec![None; (width * height) as usize],
        );
        Board {
            width,
            height,
            data,
        }
    }
}
