use crate::mino::*;
use crate::{ARENA_HEIGHT, ARENA_WIDTH};
use bevy::prelude::*;
use rand::distributions::{Distribution, Standard};
use rand::Rng;

// Startposition for new Tetriminos (about top middle of the area)

const START_X: i32 = (ARENA_WIDTH / 2) as i32;
const START_Y: i32 = (ARENA_HEIGHT - 4) as i32;

// Number of Tetriminos to keep in Bag precalculated
const TETRIMINO_BAG_SIZE: usize = 5;

#[derive(Component)]
pub struct Stone;

#[derive(Component, Clone, Copy, PartialEq, Eq, Debug)]
pub struct Position {
    pub x: i32,
    pub y: i32,
}

#[derive(Component, Clone, Copy, PartialEq, Eq, Debug)]
pub struct IsChanged {
    pub is_changed: bool,
}

#[derive(Component, Clone, Copy, PartialEq, Eq, Debug)]
pub struct BagChanged {
    pub bag_changed: bool,
}

pub fn spawn_tetrimino_base(mut commands: Commands) {
    commands
        .spawn_bundle(SpriteBundle {
            transform: Transform {
                ..Default::default()
            },
            global_transform: GlobalTransform::default(),
            ..Default::default()
        })
        .insert(Stone)
        .insert(TetriminoBag::new())
        .insert(IsChanged { is_changed: true })
        .insert(BagChanged { bag_changed: true })
        .insert(Position {
            x: START_X,
            y: START_Y,
        })
        .insert(Facing {
            direction: Rotation::N,
        })
        .id();
}

#[derive(Component)]
pub struct NextTetrimino;

pub fn spawn_next_tetrimino_base(mut commands: Commands) {
    commands
        .spawn_bundle(SpriteBundle {
            transform: Transform::default(),
            global_transform: GlobalTransform::default(),
            ..Default::default()
        })
        .insert(NextTetrimino)
        .insert(Position { x: 0, y: 0 })
        .id();
}

pub fn spawn_next_tetrimino(
    mut commands: Commands,
    mut next_tetrimino: Query<Entity, With<NextTetrimino>>,
    mut query: Query<(&TetriminoBag, &mut BagChanged), With<Stone>>,
) {
    let (tetrimino_bag, mut bag_changed) = query.iter_mut().next().unwrap();
    let parent = next_tetrimino.iter_mut().next().unwrap();
    // tuple: size, mesh_distance, offset_x, offset_y
    let next_tetrimino_display_parameter: [(f32, f32, f32, f32); TETRIMINO_BAG_SIZE - 1] = [
        (26.0, 32.0, -30.0, -12.0),
        (17.0, 22.0, -41.0, 114.0),
        (13.0, 15.0, -50.0, 202.0),
        (9.0, 10.0, -56.0, 263.0),
    ];
    if bag_changed.bag_changed {
        commands.entity(parent).despawn_descendants();
        bag_changed.bag_changed = false;
    }
    for show_tetrimino in 0..TETRIMINO_BAG_SIZE - 1 {
        for block_position in tetrimino_bag.get_rest_bag()[show_tetrimino].blocks()[0].iter() {
            commands
                .spawn_bundle(mino_bundle(
                    (block_position.0) as f32,
                    (block_position.1) as f32,
                    next_tetrimino_display_parameter[show_tetrimino].0,
                    next_tetrimino_display_parameter[show_tetrimino].1,
                    next_tetrimino_display_parameter[show_tetrimino].2,
                    next_tetrimino_display_parameter[show_tetrimino].3,
                    tetrimino_bag.get_rest_bag()[show_tetrimino].color(),
                ))
                .insert(Parent(parent));
        }
    }
}

pub fn spawn_tetrimino(
    mut commands: Commands,
    mut query: Query<(&TetriminoBag, &mut IsChanged, &Facing, Entity), With<Stone>>,
) {
    let (tetrimino, mut is_changed, direction, parent) = query.iter_mut().next().unwrap();
    if is_changed.is_changed {
        commands.entity(parent).despawn_descendants();
        for block_position in
            tetrimino.get_current_tetrimino().blocks()[direction.direction.rad()].iter()
        {
            commands
                .spawn_bundle(mino_bundle(
                    (block_position.0) as f32,
                    (block_position.1) as f32,
                    29.0,
                    35.0,
                    0.0,
                    0.0,
                    tetrimino.get_current_tetrimino().color(),
                ))
                .insert(Parent(parent));
        }
        is_changed.is_changed = false;
    }
}
// get info on the next BAG_SIZE-1 Tetriminos so store BAG_SIZE and point to the actual Tetrimino

#[derive(Component, Debug)]
pub struct TetriminoBag {
    bag: [TetriminoVariant; TETRIMINO_BAG_SIZE],
    current_tetrimino: usize,
}

impl TetriminoBag {
    pub fn new() -> Self {
        let new_bag: [TetriminoVariant; TETRIMINO_BAG_SIZE] =
            [(); TETRIMINO_BAG_SIZE].map(|_| rand::random());
        return Self {
            bag: new_bag,
            current_tetrimino: 0,
        };
    }

    pub fn next(&mut self) {
        self.bag[self.current_tetrimino] = rand::random();
        if self.current_tetrimino == TETRIMINO_BAG_SIZE - 1 {
            self.current_tetrimino = 0;
        } else {
            self.current_tetrimino += 1;
        }
    }

    pub fn get_current_tetrimino(&self) -> TetriminoVariant {
        self.bag[self.current_tetrimino]
    }

    pub fn get_rest_bag(&self) -> [TetriminoVariant; TETRIMINO_BAG_SIZE - 1] {
        let mut position = self.current_tetrimino;
        let mut return_bag = [TetriminoVariant::L; TETRIMINO_BAG_SIZE - 1];
        for i in 0..TETRIMINO_BAG_SIZE - 1 {
            if position == TETRIMINO_BAG_SIZE - 1 {
                position = 0;
            } else {
                position += 1;
            }
            return_bag[i] = self.bag[position];
        }
        return_bag
    }
}

#[derive(Component, Clone, Copy, PartialEq, Eq, Debug)]
pub enum TetriminoVariant {
    O,
    I,
    T,
    L,
    J,
    S,
    Z,
}

impl Distribution<TetriminoVariant> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> TetriminoVariant {
        match rng.gen_range(0..7) {
            0 => TetriminoVariant::I,
            1 => TetriminoVariant::T,
            2 => TetriminoVariant::L,
            3 => TetriminoVariant::J,
            4 => TetriminoVariant::S,
            5 => TetriminoVariant::Z,
            _ => TetriminoVariant::O,
        }
    }
}
impl TetriminoVariant {
    pub fn color(&self) -> MinoColor {
        match self {
            Self::O => MinoColor::Yellow,
            Self::I => MinoColor::LightBlue,
            Self::T => MinoColor::Purple,
            Self::L => MinoColor::Orange,
            Self::J => MinoColor::DarkBlue,
            Self::S => MinoColor::Green,
            Self::Z => MinoColor::Red,
        }
    }
    // return a vector with all 4 rotational possibilities of a tetrimino
    // a rotational possibility is the place of the blocks counted from the middle block
    // each tuple describes the position of the mino relative to the middle
    // as documented in the Tetris Guidelines Section A1.3
    pub fn blocks(&self) -> [Vec<(i32, i32)>; 4] {
        match self {
            Self::O => [
                vec![(0, 0), (1, 0), (1, 1), (0, 1)],
                vec![(0, 0), (1, 0), (1, 1), (0, 1)],
                vec![(0, 0), (1, 0), (1, 1), (0, 1)],
                vec![(0, 0), (1, 0), (1, 1), (0, 1)],
            ],
            Self::I => [
                vec![(-1, 1), (0, 1), (1, 1), (2, 1)],
                vec![(1, 2), (1, 1), (1, 0), (1, -1)],
                vec![(-1, 0), (0, 0), (1, 0), (2, 0)],
                vec![(0, 2), (0, 1), (0, 0), (0, -1)],
            ],
            Self::T => [
                vec![(-1, 0), (0, 0), (1, 0), (0, 1)],
                vec![(0, 0), (1, 0), (0, 1), (0, -1)],
                vec![(-1, 0), (0, 0), (1, 0), (0, -1)],
                vec![(-1, 0), (0, 0), (0, 1), (0, -1)],
            ],
            Self::L => [
                vec![(-1, 0), (0, 0), (1, 0), (1, 1)],
                vec![(0, 1), (0, 0), (0, -1), (1, -1)],
                vec![(-1, 0), (0, 0), (1, 0), (-1, -1)],
                vec![(-1, 1), (0, 1), (0, 0), (0, -1)],
            ],
            Self::J => [
                vec![(-1, 1), (-1, 0), (0, 0), (1, 0)],
                vec![(0, 1), (0, 0), (0, -1), (1, 1)],
                vec![(-1, 0), (0, 0), (1, 0), (1, -1)],
                vec![(-1, -1), (0, -1), (0, 0), (0, 1)],
            ],
            Self::S => [
                vec![(-1, 0), (0, 0), (0, 1), (1, 1)],
                vec![(0, 1), (0, 0), (1, 0), (1, -1)],
                vec![(-1, -1), (0, -1), (0, 0), (1, 0)],
                vec![(-1, 1), (-1, 0), (0, 0), (0, -1)],
            ],
            Self::Z => [
                vec![(-1, 1), (0, 1), (0, 0), (1, 0)],
                vec![(1, 1), (1, 0), (0, 0), (0, -1)],
                vec![(-1, 0), (0, 0), (0, -1), (1, -1)],
                vec![(-1, -1), (-1, 0), (0, 0), (0, 1)],
            ],
        }
    }
}

#[derive(Component, Clone, Copy, PartialEq, Eq, Debug)]
pub struct Facing {
    pub direction: Rotation,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Rotation {
    N,
    E,
    S,
    W,
}

impl Rotation {
    pub fn cw_next(&self) -> Self {
        match self {
            Self::N => Self::E,
            Self::E => Self::S,
            Self::S => Self::W,
            Self::W => Self::N,
        }
    }
    pub fn ccw_next(&self) -> Self {
        match self {
            Self::N => Self::W,
            Self::E => Self::N,
            Self::S => Self::E,
            Self::W => Self::S,
        }
    }
    pub fn rad(&self) -> usize {
        match self {
            Self::N => 0,
            Self::E => 1,
            Self::S => 2,
            Self::W => 3,
        }
    }
}
