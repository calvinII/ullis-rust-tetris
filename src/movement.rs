use crate::arena::*;
use crate::tetrimino::*;
use bevy::prelude::*;
use std::ops::DerefMut;
use std::time::Duration;

pub const START_FALLING_TIMER: f32 = 0.75;
pub const SOFT_DROP_DIVISOR: f32 = 20.0;

#[derive(Debug)]
pub struct GameState {
    falling_timer: f32,
    soft_drop_timer: f32,
    level: usize,
    score: u64,
    lines_eliminated: u64,
}

impl GameState {
    pub fn create() -> Self {
        Self {
            falling_timer: START_FALLING_TIMER,
            soft_drop_timer: START_FALLING_TIMER / SOFT_DROP_DIVISOR,
            level: 1,
            score: 0,
            lines_eliminated: 0,
        }
    }
    pub fn set_timer(&mut self, falling_timer: f32) {
        self.falling_timer = falling_timer;
        self.soft_drop_timer = falling_timer / SOFT_DROP_DIVISOR;
    }
    pub fn increase_level(&mut self) {
        self.level += 1;
    }
    pub fn add_score(&mut self, additional_score: u64) {
        self.score += additional_score;
    }
    pub fn increase_lines(&mut self) {
        self.lines_eliminated += 1;
    }
}

#[derive(Debug)]
pub struct FallTimer {
    timer: Timer,
    start_duration: f32,
}

impl FallTimer {
    pub fn create() -> Self {
        Self {
            timer: Timer::from_seconds(START_FALLING_TIMER, true),
            start_duration: START_FALLING_TIMER,
        }
    }

    pub fn set_temp_duration(&mut self, seconds: f32) {
        self.timer.set_duration(Duration::from_secs_f32(seconds));
    }

    pub fn reset_duration(&mut self) {
        self.timer
            .set_duration(Duration::from_secs_f32(self.start_duration));
    }

    pub fn set_start_duration(&mut self, seconds: f32) {
        self.timer.set_duration(Duration::from_secs_f32(seconds));
        self.start_duration = seconds;
    }
}
pub fn position_translation(
    mut q: Query<(&Position, &mut Transform), (With<Stone>, Without<TetrisField>)>,
) {
    for (pos, mut transform) in q.iter_mut() {
        transform.translation = Vec3::new(
            (pos.x * 35) as f32 + ARENA_OFFSET_X,
            (pos.y * 35) as f32 + ARENA_OFFSET_Y,
            1.0,
        );
    }
}

pub fn collision_check(
    new_position: &Position,
    variant: &TetriminoVariant,
    rotation: &Rotation,
    board: &Board,
) -> bool {
    let mut result = true;
    for block_position in variant.blocks()[rotation.rad()].iter() {
        let check_position: (i32, i32) = (
            block_position.0 + new_position.x,
            block_position.1 + new_position.y,
        );
        // Border Check
        result = match &check_position {
            (x, _) if *x < 0 => false,
            (x, _) if *x > (ARENA_WIDTH - 1) as i32 => false,
            (_, y) if *y < 0 => false,
            (_, y) if *y > (ARENA_HEIGHT) as i32 => false,
            _ => true,
        };
        if !result {
            break;
        } else {
            let t = board
                .data
                .get((check_position.0 as usize, check_position.1 as usize))
                .unwrap();
            if t.is_some() {
                result = false;
                break;
            };
        }
    }
    result
}

pub fn burn_in(
    position: &Position,
    tetrimino: &TetriminoVariant,
    rotation: &Rotation,
    mut board: Mut<'_, Board>,
) {
    for block_position in tetrimino.blocks()[rotation.rad()].iter() {
        let field = board
            .deref_mut()
            .data
            .get_mut((
                (block_position.0 + position.x) as usize,
                (block_position.1 + position.y) as usize,
            ))
            .unwrap();
        *field = Some(*tetrimino);
    }
    for i in (0..ARENA_HEIGHT as usize).rev() {
        let mut remove = true;
        for block in board.data.row_iter(i) {
            if block.is_none() {
                remove = false;
                break;
            }
        }
        if remove {
            board.data.remove_row(i);
            board.data.insert_row(
                (ARENA_HEIGHT - 1) as usize,
                vec![None; ARENA_WIDTH as usize],
            );
        }
    }
}

pub fn mino_falling(
    mut mino_positions: Query<
        (
            &mut Position,
            &mut Facing,
            &mut TetriminoBag,
            &mut IsChanged,
            &mut BagChanged,
        ),
        (With<Stone>, Without<TetrisField>),
    >,
    mut arena: Query<(&mut Board, &mut IsChanged), (With<TetrisField>, Without<Stone>)>,
    time: Res<Time>,
    mut timer: ResMut<FallTimer>,
) {
    timer
        .timer
        .tick(Duration::from_secs_f32(time.delta_seconds()));
    if timer.timer.finished() {
        // only one board, only one falling tetrimino ....
        let (mut board, mut is_changed) = arena.iter_mut().next().unwrap();
        let (mut pos, mut dir, mut tetri, mut tetri_changed, mut bag_changed) =
            mino_positions.iter_mut().next().unwrap();
        let new_pos = Position {
            x: pos.x,
            y: pos.y - 1,
        };
        if collision_check(
            &new_pos,
            &tetri.get_current_tetrimino(),
            &dir.direction,
            &board,
        ) {
            pos.y -= 1
        } else {
            burn_in(&pos, &tetri.get_current_tetrimino(), &dir.direction, board);
            pos.x = (ARENA_WIDTH / 2) as i32;
            pos.y = (ARENA_HEIGHT - 4) as i32;
            dir.direction = Rotation::N;
            tetri.next();
            is_changed.is_changed = true;
            tetri_changed.is_changed = true;
            bag_changed.bag_changed = true;
        };
    }
}

pub fn mino_movement(
    keyboard_input: Res<Input<KeyCode>>,
    mut mino_positions: Query<
        (
            &mut Position,
            &mut Facing,
            &mut TetriminoBag,
            &mut IsChanged,
            &mut BagChanged,
        ),
        (With<Stone>, Without<TetrisField>),
    >,
    mut arena: Query<(&mut Board, &mut IsChanged), (With<TetrisField>, Without<Stone>)>,
    mut timer: ResMut<FallTimer>,
) {
    let (mut pos, mut dir, mut tetri, mut is_changed, mut bag_changed) =
        mino_positions.iter_mut().next().unwrap();
    let (board, mut arena_changed) = arena.iter_mut().next().unwrap();
    if keyboard_input.just_pressed(KeyCode::Left) {
        let new_pos = Position {
            x: (pos.x) - 1,
            y: pos.y,
        };
        if collision_check(
            &new_pos,
            &tetri.get_current_tetrimino(),
            &dir.direction,
            &board,
        ) {
            pos.x -= 1
        };
    }
    if keyboard_input.just_pressed(KeyCode::Right) {
        let new_pos = Position {
            x: (pos.x) + 1,
            y: pos.y,
        };
        if collision_check(
            &new_pos,
            &tetri.get_current_tetrimino(),
            &dir.direction,
            &board,
        ) {
            pos.x += 1
        };
    }
    if keyboard_input.just_pressed(KeyCode::Up) {
        let new_dir = dir.direction.ccw_next();
        if collision_check(&pos, &tetri.get_current_tetrimino(), &new_dir, &board) {
            dir.direction = new_dir;
            is_changed.is_changed = true;
        }
    }
    if keyboard_input.just_pressed(KeyCode::Down) {
        timer.timer.set_duration(Duration::from_secs_f32(0.1));
    }
    if keyboard_input.just_released(KeyCode::Down) {
        timer
            .timer
            .set_duration(Duration::from_secs_f32(START_FALLING_TIMER));
    }

    // if keyboard_input.just_pressed(KeyCode::Down) {
    //     let new_dir = dir.direction.cw_next();
    //     if collision_check(&pos, &tetri.get_current_tetrimino(), &new_dir, &board) {
    //         dir.direction = new_dir;
    //         is_changed.is_changed = true;
    //     }
    // }
    if keyboard_input.just_pressed(KeyCode::Space) {
        let mut new_pos = Position {
            x: pos.x,
            y: (pos.y) - 1,
        };
        while collision_check(
            &new_pos,
            &tetri.get_current_tetrimino(),
            &dir.direction,
            &board,
        ) {
            new_pos = Position {
                x: pos.x,
                y: new_pos.y - 1,
            };
        }
        pos.y = new_pos.y + 1;
        burn_in(&pos, &tetri.get_current_tetrimino(), &dir.direction, board);
        pos.x = (ARENA_WIDTH / 2) as i32;
        pos.y = (ARENA_HEIGHT - 4) as i32;
        dir.direction = Rotation::N;
        tetri.next();
        is_changed.is_changed = true;
        arena_changed.is_changed = true;
        bag_changed.bag_changed = true;
    }
}
