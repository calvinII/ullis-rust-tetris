# Learning Rust / coding a game using bevy

start coding a tetris clone using the bevy engine.

resources I used while learning aside the great Rust documentation itself:

* Guidlines for creating a Tetris clone
  * https://github.com/frankkopp/Tetris/blob/master/2009%20Tetris%20Design%20Guideline.pdf
* This one was my first start into bevy. A great tutorial shows how to create a snake clone using bevy
  * https://mbuffett.com/posts/bevy-snake-tutorial/
* A reeeeeally great Youtube series from Max, showing how he learns new libraries, while writing a Pong clone
  * Part 1:  https://www.youtube.com/watch?v=j0bDFM3-71Y
* Also a great Youtube tutorial programming a tetris clone (but not using bevy ;) ):
  * Part 1: https://www.youtube.com/watch?v=74UYWFNfR64
* Background-Photo by <a href="https://unsplash.com/@von_co?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Ivana Cajina</a> on <a href="https://unsplash.com/s/photos/galaxy?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
